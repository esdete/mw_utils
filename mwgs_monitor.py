import logging
import os
import socket

from datetime import datetime

ADDR = "mw-shard.pl"
TCP_PORT = 5005
BUFFER_SIZE = 1024
TIMEOUT = 10
LOG_LEVEL = logging.INFO
LOG_FILENAME = 'mwgs_monitor.log'
CRASH = False

############################################################

logging.basicConfig(filename=LOG_FILENAME, level=LOG_LEVEL)

time = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
logging.debug("{} MW_STATUS_CHECK".format(time))

def restart_mwgs():
    os.system("killall -9 mwgs");
    os.system("./mwgs/mwgs_ctl start")

try: 
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((ADDR, TCP_PORT))

    msgs = [
        b'\x03\x04\x00\x00',
        b'\x03\x04\x00\x07',
        b'\x05\x03\x00',
    ]

    if CRASH:
        msgs = [
            b'\x03\x04\x00\x00' * 100000
        ]

    s.settimeout(TIMEOUT)

    for msg in msgs:
        s.send(msg)
        data = s.recv(BUFFER_SIZE)
        logging.debug("received: {}".format(data))

except Exception as e:
    time = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    logging.warning("{} MW_STATUS NOK {}".format(time, e))
    restart_mwgs()
    exit(-1)
finally:
    s.close()

time = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
logging.info("{} MW_STATUS_OK".format(time))
exit(0)


